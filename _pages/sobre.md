---
title: Sobre
---

# Sobre o GELOS

O **Grupo de Extensão Livre & Open Source** (**GELOS**) é um grupo de extensão
do [Instituto de Ciências Matemáticas e de Computação](https://icmc.usp.br) da
[Universidade de São Paulo](https://usp.br).

Somos um grupo recém formado, e temos como objetivo reunir pessoas interessadas
por cultura livre e open source. Bem como divulgar e fomentar esse cultura
dentro e fora da USP.

Desde software livre e hardware, até Open Science e todos os aspectos sociais
e políticos envolvidos.

O GELOS tem como princípio ser um *agrupamento de entusiastas*. Onde trocamos
conhecimento, e apoiamos (individual e institucionalmente) uns aos outros. Você
pode, ao seu critério: desenvolver ou idealizar novos projetos, contribuir com
projetos de outros membros, ou só socializar. Sendo assim, ser membro *não
implica em obrigatoriedade de atividades*, e *não existe hierarquia que "escale
membros"* para projetos.

## Como participar?
Não  temos um processo seletivo estrito, basta entrar em um dos nossos
grupinhos ou participar de alguma das nossas reuniões.  Se você quer estar
envolvido em qualquer grau, você pode se considerar um membro.

Basta entrar em um dos nossos canais ([telegram](https://telegram.gelos.club)
ou [matrix](https://matrix.gelos.club)) e você está dentro!

## GELOS e o CCOS

Somos vinculados ao Centro de Competência em Open Source (CCOS/ICMC), já com
uma tradição de 10 anos na área, que participou de vários projetos no passado:
- Sanca Livre
- Pyladies
- NAPSoL
